package org.skodia.infectedrpg.common;

import java.util.ArrayList;

public interface AsyncManyCallback<T>{
	public void doCallback(ArrayList<T> t);
}
