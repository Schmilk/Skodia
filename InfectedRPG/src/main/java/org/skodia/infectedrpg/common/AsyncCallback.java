package org.skodia.infectedrpg.common;

public interface AsyncCallback<T> {
	public void doCallback(T t);
}
