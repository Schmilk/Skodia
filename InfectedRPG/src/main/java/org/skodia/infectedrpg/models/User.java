/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.skodia.infectedrpg.models;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.skodia.infectedrpg.SpigotMain;

/**
 * @author prota
 */
public class User {

    private final UUID uuid;
    private final UserStats stats;
    private String username;


    public User(SpigotMain plugin, UUID uuid, String username) {
        this.uuid = uuid;
        this.username = username;
        this.stats = new UserStats();
    }

    public User(SpigotMain plugin, Player p) {
        // Keep on multiple lines to avoid merge conflicts
        this(plugin, p.getUniqueId(), p.getName());
    }

    public String getName() {
        return this.username;
    }

    public UUID getUuid() {
        return uuid;
    }

    public UserStats getStats() {
        return stats;
    }

    public void setName(String name) {
        this.username = name;
    }

    public static class UserStats {
        public int playersKilled = 0;
        public int playerDeaths = 0;
        // TODO: Class trees and stuff
    }
}
