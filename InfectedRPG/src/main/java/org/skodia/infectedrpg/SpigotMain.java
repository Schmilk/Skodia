/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.skodia.infectedrpg;

import org.skodia.infectedrpg.commands.TestCommand;
import org.skodia.infectedrpg.config.ConfigHelper;
import org.skodia.infectedrpg.config.Options;
import org.skodia.infectedrpg.service.CommandService;
import org.skodia.infectedrpg.utility.C;
import org.skodia.infectedrpg.listeners.PluginCommandListener;
import org.skodia.infectedrpg.listeners.JoinListener;
//import org.skodia.infectedrpg.commands.SpigotPvpModeCommand;
import org.skodia.infectedrpg.listeners.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;
import org.skodia.infectedrpg.service.DataService;
import org.skodia.infectedrpg.service.TestDataRepository;

/**
 * @author prota
 */
public class SpigotMain extends JavaPlugin {

    private DataService dataService;
    private CommandService commandService;


    @Override
    public void onEnable() {
        if (!Bukkit.getServer().getPluginManager().getPlugin(this.getName()).getDataFolder().exists()) {
            Bukkit.getServer().getPluginManager().getPlugin(this.getName()).getDataFolder().mkdir();
        }
        ConfigHelper.DATA_FOLDER = this.getDataFolder();
        Options.Load();
        Options.Save();

        getServer().getPluginManager().registerEvents(new PluginCommandListener(this), this);
        getServer().getPluginManager().registerEvents(new JoinListener(this), this);
        getServer().getPluginManager().registerEvents(new PlayerListener(this), this);

        this.dataService = new DataService(this, new TestDataRepository());
        this.dataService.onEnable();

        this.commandService = new CommandService(this);
        commandService.registerCommand(new TestCommand());

        //        getCommand("pvpmode").setExecutor(new SpigotPvpModeCommand(this));

    }

    @Override
    public void onDisable() {
        HandlerList.unregisterAll(this); // Unregister event handlers
        Bukkit.getScheduler().cancelTasks(this);
        getServer().getServicesManager().unregisterAll(this); // idr what this does
        this.dataService.onDisable();
    }

    public DataService getDataService()
    {
        return this.dataService;
    }

}
