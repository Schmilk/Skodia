/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.skodia.infectedrpg.service;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.skodia.infectedrpg.SpigotMain;
import org.skodia.infectedrpg.common.AsyncCallback;
import org.skodia.infectedrpg.common.AsyncManyCallback;
import org.skodia.infectedrpg.models.User;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author prota
 */
public class DataService {

    //<editor-fold defaultstate="collapsed" desc="General">
    private final IDataRepository repo;
    private final ConcurrentHashMap<UUID, User> onlineUsers = new ConcurrentHashMap<>();
    private final SpigotMain plugin;

    public DataService(SpigotMain p_plugin, IDataRepository p_repository) {
        this.repo = p_repository;
        this.plugin = p_plugin;
    }

    public boolean onEnable() {
        boolean success = this.repo.onEnable();
        return success;
    }

    /**
     * Non asynchronous. *
     *
     * @return
     */
    public boolean onDisable() {
        this.saveUsers(false);
        this.onlineUsers.clear(); // ^make sure the above is synchronous first.
        return this.repo.onDisable();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Saving Users">

    /**
     * Should probably run this asynchronously. *
     */
    private void saveUsers(boolean isAsync) {
        if (isAsync) {
            Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
                for (User u : onlineUsers.values()) {
                    DataService.this.repo.saveUser(u);
                }
            });
        }
        else {
            for (User u : onlineUsers.values()) {
                this.repo.saveUser(u);
            }
        }
    }


    /**
     * Save is asynchronous. If user is not writable, an illegal state exception
     * is thrown. So basically, make sure it is not being called before this is
     * initialized.
     *
     * @param user
     */
    public void saveUser(User user) {
        this.saveUser(user, true);
    }

    /**
     * Should probably run this asynchronously. *
     */
    public void saveUser(User user, boolean isAsync) {
        if (isAsync) {
            Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
                repo.saveUser(user);
            });
        }
        else {
            this.repo.saveUser(user);
        }
    }

    /**
     * Should probably run this asynchronously. Use this only when making users
     * for the first time. *
     */
    private void createUser(User user, boolean isAsync) {
        if (isAsync) {
            Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
                repo.createUser(user);
            });
        }
        else {
            this.repo.createUser(user);
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Login">

    /**
     * Must be called from a synchronous context. Creates user if not exist.
     * Otherwise updates username, ipv4, and lastPlayed time.
     *
     * @param p *
     */
    public void loginUser(final Player p, AsyncCallback<User> callback) {
        if (this.onlineUsers.containsKey(p.getUniqueId())) {
            this.onlineUsers.remove(p.getUniqueId());
        }
        if (!this.onlineUsers.containsKey(p.getUniqueId())) {
            final UUID uuid = p.getUniqueId();

            this.getOfflineUser(uuid, false, (User u) -> {

                final User nU;
                if (u != null) {
                    nU = u;
                    nU.setName(p.getName());

                    Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
                        DataService.this.saveUser(nU, true);    // Update the user into the DB.
                        Bukkit.getScheduler().runTask(plugin, () -> {
                            onlineUsers.put(uuid, nU);
                            callback.doCallback(nU);
                        });
                    });
                }
                else {
                    nU = new User(this.plugin, p);
                    Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
                        DataService.this.createUser(nU, true);    // Update the user into the DB.
                        Bukkit.getScheduler().runTask(plugin, () -> {
                            onlineUsers.put(uuid, nU);
                            callback.doCallback(nU);
                        });
                    });
                }
            });
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Logout">

    /**
     * Saves the user and removes it from the online user list. Save operation
     * happens asynchronously. Don't use this method for closing the plugin. Use
     * save all users instead.
     *
     * @param p
     */
    public void logoutUser(final Player p) {
        if (this.onlineUsers.containsKey(p.getUniqueId())) {
            User user = this.onlineUsers.remove(p.getUniqueId());
            this.saveUser(user, true);
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Get">

    /**
     * Only checks through currently online users for the data. Null if not
     * found. *
     */
    public User getUser(UUID uuid) {
        if (onlineUsers.containsKey(uuid)) {
            return onlineUsers.get(uuid);
        }
        return null;
    }

    /**
     * Attempts to resolve the partial name into a full name of an online
     * player. If found, the User is returned. Otherwise null is returned. Only
     * checks results from online users. Will make no calls to file system for
     * the data.
     *
     * @param partialUsername
     * @return
     */
    public User getUserBestOnlineMatch(String partialUsername) {
        synchronized (this.onlineUsers) {
            if (partialUsername == null) {
                return null;
            }
            String name = partialUsername.toLowerCase();
            User exactMatch = null;
            User bestPartialMatch = null;
            String bestPartialMatchStr = null;

            // I am fully aware that this function could be made more efficient. Total size of the collection will 
            // never be above 2000 though, so any minor efficiency glitches can be ignored. 3N would still be fine.

            // Exact
            // RealPartial
            // NickPartial
            // Any ties in length are won by the item that comes first in this list.
            for (User u : this.onlineUsers.values()) {
                if (name.equalsIgnoreCase(u.getName())) {
                    exactMatch = u;
                    break;
                }

                String realName = ChatColor.stripColor(u.getName()).toLowerCase();
                if (realName.startsWith(name)) {
                    if (bestPartialMatch == null) {
                        bestPartialMatch = u;
                        bestPartialMatchStr = realName;
                    }
                    else if (realName.length() <= bestPartialMatchStr.length()) {
                        bestPartialMatch = u;
                        bestPartialMatchStr = realName;
                    }
                }
            }

            if (exactMatch != null) {
                return exactMatch;
            }

            if (bestPartialMatch != null) {
                return bestPartialMatch;
            }

            return null;
        }
    }


    /**
     * IO heavy operation. Loads the user from the database. Run this
     * asynchronously. if useCache is true, users can be loaded from the online
     * users hashmap before being taken from the database.
     *
     * @param uuid
     * @param useCache
     * @param callback
     * @return *
     */
    public void getOfflineUser(UUID uuid, boolean useCache, AsyncCallback<User> callback) {
        if (useCache) {
            if (this.onlineUsers.containsKey(uuid)) {
                User u = this.onlineUsers.get(uuid);
                callback.doCallback(u);
                return;
            }
        }
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            User u = this.repo.getUser(uuid);
            Bukkit.getScheduler().runTask(plugin, () -> {
                callback.doCallback(u);
            });
        });
    }

    /**
     * IO heavy operation. Loads the user from the database. Run this
     * asynchronously. if useCache is true, users can be loaded from the online
     * users hashmap before being taken from the database.
     *
     * @param username
     * @param callback
     * @param useCache
     * @return *
     */
    public void getOfflineUser(String username, boolean useCache, AsyncCallback<User> callback) {
        if (useCache) {
            synchronized (this.onlineUsers) {
                for (User u : this.onlineUsers.values()) {
                    if (u.getName().equalsIgnoreCase(username)) {
                        callback.doCallback(u);
                        return;
                    }
                }
            }
        }
        this.getOfflineUsers(username, useCache, (ArrayList<User> users) -> {
            int shortestLen = Integer.MAX_VALUE;
            User tmp = null;
            for (int i = 0; i < users.size(); i++) {
                User u = users.get(i);
                if (u.getName().length() < shortestLen) {
                    shortestLen = u.getName().length();
                    tmp = u;
                }
            }
            callback.doCallback(tmp);
        });
    }

    /**
     * IO heavy operation. Loads the user from the database. Run this
     * asynchronously. if useCache is true, users can be loaded from the online
     * users hashmap before being taken from the database.
     *
     * @param username
     * @param callback
     * @param useCache
     * @return *
     */
    public void getOfflineUserByNameOrDisplayName(String username, AsyncCallback<User> callback) {
        User bestMatch = this.getUserBestOnlineMatch(username);
        if (bestMatch != null) {
            callback.doCallback(bestMatch);
        }
        else {
            this.getOfflineUsers(username, false, (ArrayList<User> users) -> {
                int shortestLen = Integer.MAX_VALUE;
                User tmp = null;
                for (int i = 0; i < users.size(); i++) {
                    User u = users.get(i);
                    if (u.getName().length() < shortestLen) {
                        shortestLen = u.getName().length();
                        tmp = u;
                    }
                }
                callback.doCallback(tmp);
            });
        }
    }

    /**
     * IO heavy operation. Loads the user from the database. Run this
     * asynchronously. if useCache is true, users can be loaded from the online
     * users hashmap before being taken from the database.
     *
     * @param username
     * @param callback
     * @param useCache
     * @return *
     */
    public void getOfflineUsers(String username, boolean useCache, AsyncManyCallback<User> callback) {
        if (useCache) {
            synchronized (this.onlineUsers) {
                for (User u : this.onlineUsers.values()) {
                    if (u.getName().equalsIgnoreCase(username)) {
                        ArrayList<User> us = new ArrayList<User>();
                        us.add(u);
                        callback.doCallback(us);
                        return;
                    }
                }
            }
        }

        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            ArrayList<User> us = this.repo.getUsers(username);
            Bukkit.getScheduler().runTask(plugin, () -> {
                callback.doCallback(us);
            });
        });
    }

    //</editor-fold>
}
