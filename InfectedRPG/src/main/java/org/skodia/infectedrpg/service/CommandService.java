package org.skodia.infectedrpg.service;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.skodia.infectedrpg.commands.generic.GenericCommand;
import org.skodia.infectedrpg.utility.attributes.SkodiaCommand;

import javax.annotation.Nonnull;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author AvroVulcan
 */
public class CommandService implements CommandExecutor {

    private JavaPlugin main;

    CommandMap commandMap;
    private Map<Command, Map.Entry<Method, Object>> registeredCommands;

    public CommandService(JavaPlugin main) {
        this.main = main;

        registeredCommands = new ConcurrentHashMap<>();

        try {
            Field bukkitCommandMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            bukkitCommandMap.setAccessible(true);
            commandMap = (CommandMap) bukkitCommandMap.get(Bukkit.getServer());
        }
        catch (NoSuchFieldException | IllegalAccessException err) {
            err.printStackTrace();
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Method invoke = registeredCommands.get(cmd).getKey();
        Object object = registeredCommands.get(cmd).getValue();
        SkodiaCommand command = invoke.getAnnotation(SkodiaCommand.class);

        if(!(sender.hasPermission(command.permission()))) {
            sender.sendMessage("No permission!");
            return true;
        }
        if(command.playerOnly() && !(sender instanceof Player)) {
            sender.sendMessage("You mustn't be console!");
            return true;
        }

        try {
            invoke.invoke(object, sender, label, args);
        }
        catch (IllegalAccessException | InvocationTargetException err) {
            err.printStackTrace();
            return true;
        }

        return true;
    }

    public boolean registerCommand(@Nonnull Object command) {
        Class commandClass = command.getClass();
        Method[] methods = commandClass.getMethods();

        for (Method method : methods) {
            if (method.getAnnotation(SkodiaCommand.class) != null) {
                SkodiaCommand annotation = method.getAnnotation(SkodiaCommand.class);
                GenericCommand mainCommand = new GenericCommand(this, main, annotation.name(), annotation.description(), annotation.usage(), Arrays.asList(annotation.aliases()));

                registeredCommands.put(mainCommand, new AbstractMap.SimpleEntry<>(method, command));
                commandMap.register(annotation.name(), main.getName().toLowerCase(), mainCommand);

                return true;
            }
        }
        return false;
    }

    //TODO: Unregister command

}
