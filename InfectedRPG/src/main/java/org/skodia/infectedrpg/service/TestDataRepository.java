/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.skodia.infectedrpg.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import org.skodia.infectedrpg.models.User;

/**
 *
 * @author prota
 */
public class TestDataRepository implements IDataRepository {

    HashMap<UUID, User> users = new HashMap<UUID, User>();
    
    @Override
    public boolean onEnable() {
        return true;
    }

    @Override
    public boolean onDisable() {
        return true;
    }

    /**
     * Returns null if not found
     * @param uniqueId
     * @return 
     */
    @Override
    public User getUser(UUID uniqueId) {
        return users.get(uniqueId);
    }

    @Override
    public boolean saveUser(User user) {
        this.users.put(user.getUuid(), user);
        return true;
    }

    @Override
    public ArrayList<User> getUsers(String username) {
        return new ArrayList<User>(this.users.values());
    }

    @Override
    public void createUser(User user) {
        this.users.put(user.getUuid(), user);
    }
    
}
