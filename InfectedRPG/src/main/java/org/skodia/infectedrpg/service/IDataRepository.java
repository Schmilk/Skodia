package org.skodia.infectedrpg.service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;
import org.skodia.infectedrpg.models.User;

/**
 *
 * @author Taylor Love (Pangamma)
 */
public interface IDataRepository {
	
	//<editor-fold defaultstate="collapsed" desc="Instance">
	/** Opening connections, initializing databases or files, etc **/
    public boolean onEnable();
	
	/** Closing hanging connections, saving files, etc. **/
	public boolean onDisable();
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Users">
	
	public User getUser(UUID uniqueId);
	
	public boolean saveUser(User user);
	
	public ArrayList<User> getUsers(String username);
	
	public void createUser(User user);
	
	//</editor-fold>
	

}
