package org.skodia.infectedrpg.spigot;

import java.io.InputStream;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

/**
 * @author Taylor Love (Pangamma)
 */
public class STATIC {

    //</editor-fold>

    /**
     * returns null if display name or regular name are not found. within online
     * players list.
     */
    public static Player getPlayer(String name) {
        name = name.toLowerCase();
        Player p = Bukkit.getPlayer(name);
        if (p != null && p.isOnline()) {
            return p;
        }
        
        for (Player n : Bukkit.getOnlinePlayers()) {
            String nick = ChatColor.stripColor(n.getDisplayName()).toLowerCase();
            if (nick.contains(name)) {
                return n;
            }
        }
        return null;
    }


    @Deprecated
    /**
     * returns null if display name or regular name are not found. *
     */
    public static OfflinePlayer getOfflinePlayer(String name) {
        name = name.toLowerCase();
        OfflinePlayer op = Bukkit.getOfflinePlayer(name);
        if (op != null && op.hasPlayedBefore()) {
            return op;
        }
        OfflinePlayer[] list = Bukkit.getOfflinePlayers();
        for (int i = 0; i < list.length; i++) {
            op = list[i];
            if (op.getName().toLowerCase().startsWith(name)) {
                return op;
            }
        }
        return null;
    }

}
