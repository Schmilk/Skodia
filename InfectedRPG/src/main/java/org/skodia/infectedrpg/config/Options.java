/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.skodia.infectedrpg.config;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import org.skodia.infectedrpg.models.Hyperlink;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Idk. Sometimes I just disagree with Java's naming conventions. Properties are great, alright? C# is bae.
 * @author prota
 */
public class Options
{
    //<editor-fold defaultstate="collapsed" desc="Properties">
    @SerializedName("is-enhanced-plugin-list-enabled")
    public boolean IsEnhancedPluginListEnabled = true;
    
    @SerializedName("pvp-mode")
    public PvpModeOption PvpMode = new PvpModeOption();
    //</editor-fold>

    
    //<editor-fold defaultstate="collapsed" desc="SubClasses">
  
    public static class DynmapOption{
        
        @SerializedName("enabled")
        public boolean IsEnabled = false;
        
        @SerializedName("links")
        public ArrayList<Hyperlink> Links = new ArrayList<>();
        
        public DynmapOption(){
            Links.add(new Hyperlink("&f&nClick to view Dynmap", "https://maps.yoursite.net:8123", "Click to open."));
        }
    }    
    
    public static class PvpModeOption{
        
        @SerializedName("enabled")
        public boolean IsEnabled = true;
        
        @SerializedName("enable-glow")
        public boolean IsGlowEffectEnabled = false;
        
        public PvpModeOption(){
        }
    }
    
    public static class ChatSystemOptions {

        @SerializedName("enabled")
        public boolean IsEnabled = true;
        
        @SerializedName("ignore-list-limit")
        public int MaxIgnoreListSize = 10;
        
        @SerializedName("allow-me-on-main-channel")
        public boolean IsMeAllowedOnMainChannel = true;
        
        public ChatSystemOptions() {
        }
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="CORE methods">
    private static final String fileName = "config.json";
    /** 
     * Java sucks compared to C#. I miss my properties!
     * 
     * @return Options
     */
    public static Options Get(){
        if (_get == null){
            _get = Options.Load();
        }
        return _get;
    }
    private static Options _get = null;
    
    public static Options Save(){
        Options o = Get();
        ConfigHelper.SaveJson(o, fileName);
        return _get;
    }
    
    public static Options Load(){
        String json = ConfigHelper.LoadJson(fileName);
        try{
            Gson gson = new Gson();
            if (json != null){
                Options fromJson = gson.fromJson(json, Options.class);
                _get = fromJson;
            }
        }catch(Exception ex){
            Logger.getLogger(ConfigHelper.class.getName()).log(Level.SEVERE, "Failed to load config '"+fileName+"'", ex);
        }
        if (_get == null){
            _get = new Options();
        }
        return _get; 
    }


    public Options() {
    }
    //</editor-fold>

}
