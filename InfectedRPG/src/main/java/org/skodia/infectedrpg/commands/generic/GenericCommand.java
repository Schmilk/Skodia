package org.skodia.infectedrpg.commands.generic;

import org.bukkit.command.Command;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import java.util.List;

/**
 * @author AvroVulcan
 */
public class GenericCommand extends Command {

    private Plugin owner;
    private CommandExecutor executor;

    public GenericCommand(CommandExecutor executor, Plugin owner, String name, String description, String usageMessage, List<String> aliases) {
        super(name, description, usageMessage, aliases);
        this.owner = owner;
        this.executor = executor;
    }

    @Override
    public boolean execute(CommandSender sender, String label, String[] args) {
        boolean success = false;

        if (!owner.isEnabled()) {
            return false;
        }

        try {
            success = executor.onCommand(sender, this, label, args);
        }
        catch (Throwable ex) {
            throw new CommandException("Unhandled exception executing command '" + label + "' in plugin " + owner.getDescription().getFullName(), ex);
        }

        return success;
    }


}
