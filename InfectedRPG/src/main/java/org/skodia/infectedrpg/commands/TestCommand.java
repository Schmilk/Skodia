package org.skodia.infectedrpg.commands;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.skodia.infectedrpg.utility.attributes.SkodiaCommand;

/**
 * @author AvroVulcan
 */
public class TestCommand {

    @SkodiaCommand(name = "givestackdiamondpls", description = "test", usage = "hello", permission = "test.test", playerOnly = true)
    public void test(CommandSender sender, String label, String[] args) {
        Player p = (Player) sender;
        p.getInventory().addItem(new ItemStack(Material.DIAMOND, 64));
    }
}
