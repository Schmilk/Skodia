/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.skodia.infectedrpg.utility;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandSender;


//<editor-fold defaultstate="collapsed" desc="permissions">

public enum Permissions {

    ALL("Scodia.*"),
    VIEW_PLUGINS("bukkit.command.plugins");
    ;
    
    //<editor-fold defaultstate="collapsed" desc="methods">
    public String node = "Scodia.*";

    /**
     * returns the node.*
     */
    @Override
    public String toString() {
        return node;
    }

    private Permissions(String node) {
        this.node = node;
    }

    public static String TELL_USER_PERMISSION_THEY_LACK(Permissions node) {
        return TELL_USER_PERMISSION_THEY_LACK(node.node);
    }

    public static String TELL_USER_PERMISSION_THEY_LACK(String node) {
        return ChatColor.RED + "Oh teh noes! D: It appears you lack the '" + node + "' permission node that is required to perform this operation.";
    }

    /**
     * Tells user if they lack permissions.
     *
     * @param cs
     * @param node
     * @return *
     */
    public static boolean USER_HAS_PERMISSION(CommandSender cs, Permissions node) {
        return USER_HAS_PERMISSION(cs, node, true);
    }

    public static boolean USER_HAS_PERMISSION(CommandSender cs, Permissions node, boolean tellIfLacking) {

        if (cs.isOp()) {
            return true;
        }

        if (cs.hasPermission(node.node)) {
            return true;
        }

        String[] args = node.node.split("\\.");
        if (args.length > 0) {
            String perm = "";
            for (int i = 0; i < args.length - 1; i++) {
                if (i > 0) {
                    perm += "." + args[i];
                } else {
                    perm = args[i];
                }
                if (cs.hasPermission(perm + ".*")) {
                    return true;
                }
            }
        }

        if (tellIfLacking) {
            cs.sendMessage(ChatColor.RED + "Oh teh noes! D: It appears you lack the '" + node + "' permission node that is required to perform this operation.");
        }

        return false;
    }
    //</editor-fold>
}
