package org.skodia.infectedrpg.utility.attributes;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author AvroVulcan
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SkodiaCommand {

    String name();

    String description();

    String usage();

    String permission();

    boolean playerOnly();

    String[] aliases() default {};

}